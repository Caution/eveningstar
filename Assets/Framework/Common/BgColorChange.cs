﻿using UnityEngine;
using System.Collections;

public class BgColorChange : MonoBehaviour {

	// このスクリプトの使い方 ==========================
	
	// ①
	// このスクリプトをシーン内のオブジェクトにコンポーネントする

	// ②
	// インスペクターに<chageColor>を設定
	// <changeColor / Color> : 色
	
	// ================================================= 

	public Color changeColor;

	// Use this for initialization
	void Start () {
	
		Camera.main.backgroundColor = changeColor;
	}

    // Update is called once per frame
    void Update() {

	}
}
