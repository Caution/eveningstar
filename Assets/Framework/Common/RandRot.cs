﻿using UnityEngine;
using System.Collections;

public class RandRot : MonoBehaviour {

    public bool x = false;
    public bool y = false;
    public bool z = false;

	// Use this for initialization
	void Start () {
        Vector3 temp = Vector3.zero;
        if (x)
            temp.x = Random.Range(0.0f, 360.0f);
        if (y)
            temp.y = Random.Range(0.0f, 360.0f);
        if (z)
            temp.z = Random.Range(0.0f, 360.0f);

        transform.Rotate(temp);
    }
	
}
