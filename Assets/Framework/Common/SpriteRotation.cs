﻿using UnityEngine;
using System.Collections;

public class SpriteRotation : MonoBehaviour {

	public Vector3 value;

	// Use this for initialization
	void Start () {
	
	}

    // Update is called once per frame
    void Update () {
		transform.eulerAngles += value * GameStatusManager.Instance.delta_time_;
	}
}
