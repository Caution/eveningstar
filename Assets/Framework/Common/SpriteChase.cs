﻿using UnityEngine;
using System.Collections;

public class SpriteChase : MonoBehaviour {
	
	public Vector3 offset_position;
	public string target_object_name = "player";

	private bool is_chase;

	// Use this for initialization
	void Start () {
		is_chase = true;
	
	}

    // Update is called once per frame
    void Update () {
		if(is_chase)
		{
			GameObject player = GameObject.Find(target_object_name);
			Vector3 trance_position = player.transform.position;
			trance_position += offset_position * GameStatusManager.Instance.delta_time_;
			transform.position = trance_position;	
		}
		else
		{
			transform.position = new Vector3(0.0f,0.0f,100.0f);	
		}
	}

	public void SetChase(bool flag)
	{
		is_chase = flag;
	}
}