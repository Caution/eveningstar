﻿using UnityEngine;
using System.Collections;

public class RoundMove : MonoBehaviour {

    public float move_speed_ = 0.1f;
    public float move_renge_ = 2.0f;

    private Vector3 first_position_;
    private float time_ = 0.0f;

    // Use this for initialization
    void Start() {
        first_position_ = transform.position;
        time_ = 0.0f;
    }

    // Update is called once per frame
    void Update() {
        time_ += move_speed_ * GameStatusManager.Instance.delta_time_;
        float pos_x = Mathf.Sin(time_) * move_renge_;
        transform.position = first_position_ + Vector3.right * pos_x;

    }

    public void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Player")
        {
            col.gameObject.transform.parent = gameObject.transform;
        }
    }

    public void OnCollisionExit(Collision col)
    {
        if (col.gameObject.tag == "Player")
        {
            col.gameObject.transform.parent = null;
        }
    }
}