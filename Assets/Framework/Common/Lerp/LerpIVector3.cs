﻿using UnityEngine;
using System.Collections;

public class LerpVector3: LerpItem {

    private Vector3 begin_;
    private Vector3 end_;

    public LerpVector3(float speed):base(speed) {

    }

    public Vector3 Lerp() {
        return Vector3.Lerp(begin_, end_, MyMath.Easing(time_));
    }

    public void SetPalamater(Vector3 begin, Vector3 end) {
        begin_ = begin;
        end_ = end;
    }
}
