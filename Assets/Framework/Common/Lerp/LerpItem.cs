﻿using UnityEngine;
using System.Collections;

public class LerpItem {

    protected float time_;

    public float speed_ { get; set; }

    public float min_ { get; set; }

    public float max_ { get;set;}

    public bool is_end_ { get; private set; }

    public LerpItem(float speed) {
        time_ = 0.0f;
        speed_ = speed;
        min_ = 0.0f;
        max_ = 1.0f;
    }

    public void SpeedRebirth() {
        speed_ = -speed_;

    }

    public void Update() {
        time_ += speed_;

        // 完了したか否か
        if (speed_ > 0)
            is_end_ = time_ > max_;
        else
            is_end_ = time_ < min_;

        time_ = Mathf.Min(Mathf.Max(time_, min_), max_);
    }

    public void DeltaTimeUpdate() {
        time_ += speed_ * GameStatusManager.Instance.delta_time_;

        // 完了したか否か
        if(speed_ > 0)
            is_end_ = time_ > max_;
        else
            is_end_ = time_ < min_;

        time_ = Mathf.Min(Mathf.Max(time_, min_), max_);
    }
}
