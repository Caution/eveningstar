﻿using UnityEngine;
using System.Collections;

public class SpriteFlash : MonoBehaviour {

	// local parameter =========================
	// public parameter
	public float FadeSpeed;
	public Color color;

	// private parameter
	private float FadeAlpha;

	// Use this for initialization
	void Start () {
		FadeAlpha = 0.0f;
	}

    // Update is called once per frame
    void Update () {

		FadeAlpha += FadeSpeed;
		if( FadeAlpha > Mathf.PI )
		{
			FadeAlpha -= Mathf.PI * 2.0f;
		}
		else if( FadeAlpha < -Mathf.PI )
		{
			FadeAlpha += Mathf.PI * 2.0f;
		}
		float alpha = 0.5f + ( Mathf.Sin (FadeAlpha) ) * 0.5f * GameStatusManager.Instance.delta_time_;

        if (GetComponent<SpriteRenderer>() != null)
        {
            SpriteRenderer sr = GetComponent<SpriteRenderer>();
            sr.color = new Color(color.r, color.g, color.b, alpha);
        }
        else if (GetComponent<TextMesh>() != null)
        {
            TextMesh sr = GetComponent<TextMesh>();
            sr.color = new Color(color.r, color.g, color.b, alpha);
        }


	}
}
