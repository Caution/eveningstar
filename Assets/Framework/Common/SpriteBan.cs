﻿using UnityEngine;
using System.Collections;

public class SpriteBan : MonoBehaviour {

    public Vector3 target_position;
    public Vector3 target_scaling;
    public float ban_speed;

    private Vector3 first_position;
    private Vector3 first_scaling;
    private bool is_ban;

	// Use this for initialization
	void Start () {
        is_ban = false;
	    first_position = transform.localPosition;
        first_scaling = transform.localScale;
	}

    void Update() {

        var speed = ban_speed * GameStatusManager.Instance.delta_time_;
        if (!is_ban)
        {
            transform.localPosition += (first_position - transform.localPosition) * speed;
            transform.localScale += (first_scaling - transform.localScale) * speed;
        }
        else
        {
            transform.localPosition += (target_position - transform.localPosition) * speed;
            transform.localScale += (target_scaling - transform.localScale) * speed; 
        }
    }

    void OnMouseDown() {
        is_ban = !is_ban;
    }
}
