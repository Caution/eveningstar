﻿using UnityEngine;
using System.Collections;

public class SpriteBeat : MonoBehaviour {

	public float beat_max;
	public float beat_min;
	public Vector3 beat_speed;
	private bool is_atomization;

	// Use this for initialization
	void Start () {
		is_atomization = true;
	
	}

    // Update is called once per frame
    void Update () {
		if(is_atomization)
		{
			if(transform.localScale.x < beat_max)
			{
				transform.localScale += beat_speed * GameStatusManager.Instance.delta_time_;
			}
			else
			{
				is_atomization = false;
			}
		}
		else
		{
			if(transform.localScale.x > beat_min)
			{
				transform.localScale -= beat_speed * GameStatusManager.Instance.delta_time_;
			}
			else
			{
				is_atomization = true;
			}
		}

	}
}
