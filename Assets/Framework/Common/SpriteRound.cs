﻿using UnityEngine;
using System.Collections;

public class SpriteRound : MonoBehaviour {
	
	public int max_flame;
	public Vector3 add_value;

	private int current_flame;
	private int round;
	private Vector3 start_position;
	private Vector3 offset_position;

	// Use this for initialization
	void Start () {
		current_flame = 0;
		round = 1;
		offset_position = Vector3.zero;
		start_position = transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
		current_flame ++;
		if(current_flame == max_flame)
		{
			current_flame = 0;
			round *= -1;
		}

		offset_position += add_value * round * GameStatusManager.Instance.delta_time_;

        transform.localPosition = start_position + offset_position;
	}
}
