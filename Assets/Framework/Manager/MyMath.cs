﻿using UnityEngine;
using System.Collections;

public class MyMath : MonoBehaviour
{

    static public float Easing(float t)
    {
        t = Mathf.Min(Mathf.Max(t, 0.0f), 1.0f);

        if (t < 0.5f)
        {
            float temp = 2 * t;
            return (temp * temp * temp) * 0.5f;
        }
        else
        {
            float temp = 2 * t - 2;
            return (temp * temp * temp) * 0.5f + 1.0f;
        }

    }

}