﻿using UnityEngine;
using System.Collections;


public class MoneyManager : SingletonMonoBehaviour<MoneyManager> {

    public int GetMoney()
    {
        var temp = SaveManager.Instance;
        return temp.save_data_.money_;
    }

    public void AddMoney(int value)
    {
        SaveManager.Instance.save_data_.money_ += value;
    }

    public void SetMoney(int money)
    {
        SaveManager.Instance.save_data_.money_ = money;
    }
}
