﻿using UnityEngine;
using System.Collections;

public class StartManager : SingletonMonoBehaviour<StartManager>
{
    public GameObject[] create_objects_;
    public string current_scene_;
    public bool is_absolute = false;


    // Use this for initialization
    void Start()
    {
        StartStage();
    }

    void OnLevelWasLoaded()
    {
        StartStage();
    }

    private void StartStage()
    {
        foreach(var item in create_objects_)
        {
            var start_position = SaveManager.Instance.save_data_.start_position_;
            current_scene_ = SaveManager.Instance.save_data_.stage_;
            GameObject obj = (GameObject)Instantiate(item, start_position, Quaternion.identity);
            obj.transform.name = item.name;
        }
    }

    public void SetStartPosition(Vector3 start_posiiton)
    {
        SaveManager.Instance.save_data_.start_position_ = start_posiiton;
    }

}
