﻿using UnityEngine;
using System.Collections;

public class GameStatusManager : SingletonMonoBehaviour<GameStatusManager>
{
    private float start_game_speed_;
    public bool is_game_active_;
    public float delta_time_;

    public void Start()
    {
        start_game_speed_ = Time.timeScale;
        is_game_active_ = true;
    }

    void Update()
    {
        delta_time_ = Time.deltaTime * 60.0f;
    }

    public void ResetGameSpeed()
    {
        SetGameSpeed(start_game_speed_);
    }

    public void StopGame()
    {
        SetGameSpeed(0);
        is_game_active_ = false;
    }

    public void SetGameSpeed(float speed)
    {
        Time.timeScale = speed;
    }

    public void ActivateGame()
    {
        ResetGameSpeed();
        is_game_active_ = true;
    }

    static public void EndGame()
    {
        UnityEngine.Application.Quit();
    }

    static public Vector2 ScreenCenter()
    {
        return new Vector2(Screen.width / 2, Screen.height / 2);
    }

    static public void LoadScene(string scene_name)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(scene_name);
    }

    static public string CurrentScene()
    {
        return UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
    }

}
