﻿using UnityEngine;
using System.Collections;

public class ADVImage
{

    public Color color_;
    public bool is_color_chenge_;

    public Sprite sprite_;
    public bool is_sprite_chenge;

    public bool is_enable_;

    public bool is_disable_;

    public ADVImage()
    {
        is_enable_ = false;
        is_disable_ = false;

        color_ = Color.clear;
        is_color_chenge_ = false;

        sprite_ = null;
        is_sprite_chenge = false;
    }

    public void SetColor(Color value)
    {
        color_ = value;
        is_color_chenge_ = true;
    }

    public void SetSprite(Sprite sprite)
    {
        sprite_ = sprite;
        is_sprite_chenge = true;
    }

}
