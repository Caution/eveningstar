﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ADVSection
{
    public string sentence_;
    public ADVImage[] image_ = new ADVImage[3];

    public ADVSection(string sentence)
    {
        sentence_ = sentence;
        for (var i = 0; i < image_.Length; i++)
            image_[i] = new ADVImage();
    }

    public ADVSection(string sentence, ADVImage[] image)
    {
        sentence_ = sentence;

        for (var i = 0; i < image_.Length; i++)
            image_[i] = image[i];
    }

}
