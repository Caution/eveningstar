﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class ADVChapter
{
    public string name_;
    public ADVSection[] sections_;

    public ADVChapter(string name, string scenerio)
    {
        name_ = name;

        var data = scenerio.Replace("\n", "").Replace("<er>", "\n");
        var sections = data.Split('\n');
        sections_ = CreateSections(sections);
    }

    ADVSection[] CreateSections(string[] sections)
    {
        var result = new List<ADVSection>();
        var tag_regex = new Regex("<.*?>", RegexOptions.Singleline);
        foreach (var item in sections)
        {
            if (string.IsNullOrEmpty(item) || item == "\r")
                continue;

            var sentence = tag_regex.Replace(item.Replace("<r>", "\n"), "");

            var images = new List<ADVImage>();
            images.Add(CreateImage(TagValue(item, "<image0", ">")));
            images.Add(CreateImage(TagValue(item, "<image1", ">")));
            images.Add(CreateImage(TagValue(item, "<image2", ">")));
            SetHighlight(images, item);
            result.Add(new ADVSection(sentence, images.ToArray()));
        }
        return result.ToArray();
    }

    ADVImage CreateImage(string sentence)
    {
        var result = new ADVImage();
        SetColor(result, sentence);
        SetSprite(result, sentence);
        SetDisable(result, sentence);
        return result;
    }

    void SetColor(ADVImage image, string sentence)
    {
        var value = TagValue(sentence, "color=", "|");
        if (!string.IsNullOrEmpty(value))
        {
            SetEnable(image, sentence);

            switch (value)
            {
                case "white": image.SetColor(Color.white); break;
                case "black": image.SetColor(Color.black); break;
                case "clear": image.SetColor(Color.clear); break;
                case "gray": image.SetColor(Color.gray); break;
                default:
                    var colors = value.Split(',');
                    var color = new Color();
                    float.TryParse(colors[0], out color.r);
                    float.TryParse(colors[1], out color.g);
                    float.TryParse(colors[2], out color.b);
                    float.TryParse(colors[3], out color.a);
                    image.SetColor(color);
                    break;
            }
        }
    }

    void SetSprite(ADVImage image, string sentence)
    {
        var value = TagValue(sentence, "sprite=", "|");
        if (!string.IsNullOrEmpty(value))
        {
            SetEnable(image, sentence);

            var sprite = Resources.Load<Sprite>("Charcter/" + value);
            image.SetSprite(sprite);
        }
    }

    void SetDisable(ADVImage image, string sentence)
    {
        if (sentence.IndexOf("disable") > -1)
            image.is_disable_ = true;
    }

    void SetEnable(ADVImage image, string sentence)
    {
        image.is_enable_ = true;
    }

    void SetHighlight(List<ADVImage> images, string sentence)
    {
        var value = TagValue(sentence, "<highlight=", ">");

        var index = -1;
        if (int.TryParse(value, out index))
        {
            for (var i = 0; i < images.Count; i++)
            {
                if (index == i)
                    images[i].SetColor(Color.white);
                else
                    images[i].SetColor(Color.gray);
            }
        }
    }

    string TagValue(string target, string start, string end)
    {
        var result = target.Clone() as string;

        var first = result.IndexOf(start);
        if (first > -1)
        {
            result = result.Substring(first);
            result = result.Substring(0, result.IndexOf(end));
        }
        else
            return string.Empty;

        result = result.Replace(start, string.Empty);
        return result;
    }
}
