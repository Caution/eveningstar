﻿using UnityEngine;
using UnityEngine.UI;

public class ADVController : MonoBehaviour {
    public Image[] image_;
    public Text message_;
    public float message_speed_ = 0.1f;
    public float pop_speed_ = 0.1f;

    private bool pop_end_;
    private float pop_lerp_;
    private ADVSection[] section_;

    private string stack_message_;

    private int current_cursol_;
    private int current_section_id_ = 0;
    private float message_counter_ = 0.0f;

    private string chapter_name_;
    private bool is_adv_ = false;

    // Use this for initialization
    void Awake() {
        is_adv_ = false;
        pop_end_ = false;
        pop_lerp_ = 0.0f;
        chapter_name_ = "";

        stack_message_ = "";
        current_cursol_ = 0;
        message_counter_ = 0.0f;

    }

    // Update is called once per frame
    void Update() {
        if (pop_end_)
        {
            UpdateCursol();
            UpdateMessage();
        }

        WindowAnimation();
    }

    private void UpdateCursol() {
        if (current_cursol_ < stack_message_.Length)
        {
            if (Time.time > message_counter_ + message_speed_)
            {
                message_counter_ = Time.time;
                current_cursol_++;
            }
        }
    }

    private void UpdateMessage() {
        message_.text = stack_message_.Substring(0, current_cursol_);

    }

    private void WindowAnimation() {
        if (is_adv_)
            pop_lerp_ += pop_speed_;
        else
            pop_lerp_ -= pop_speed_;

        pop_end_ = (pop_lerp_ > 1.0f);
        pop_lerp_ = Mathf.Min(Mathf.Max(pop_lerp_, 0.0f), 1.0f);
        transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, MyMath.Easing(pop_lerp_));
    }


    private void SetSection(ADVSection section) {
        SetImage(section);
        SetMessage(section);
    }

    private void SetImage(ADVSection section) {
        for (var i = 0; i < image_.Length; i++)
        {
            if (section.image_[i].is_disable_)
                image_[i].enabled = false;
            if (section.image_[i].is_enable_)
                image_[i].enabled = true;
            if (section.image_[i].is_color_chenge_)
                image_[i].color = section.image_[i].color_;
            if (section.image_[i].is_sprite_chenge)
                image_[i].sprite = section.image_[i].sprite_;
        }
    }

    private void SetMessage(ADVSection section) {
        message_.text = "";
        stack_message_ = section.sentence_;
        current_cursol_ = 1;
        message_counter_ = Time.time;
    }

    public void SetADV(ADVChapter chapter) {
        chapter_name_ = chapter.name_;
        section_ = chapter.sections_;

        is_adv_ = true;
        current_section_id_ = 0;
        SetSection(section_[0]);
    }

    public void NextSection() {
        if (current_cursol_ < stack_message_.Length)
            current_cursol_ = stack_message_.Length;
        else
            EndSection();
    }

    private void EndSection() {
        if (!pop_end_)
            return;

        current_section_id_++;
        if (current_section_id_ == section_.Length)
            is_adv_ = false;
        else
            SetSection(section_[current_section_id_]);
    }

    public bool GetIsADV() {
        return is_adv_;
    }

    public string GetChapterName() {
        return chapter_name_;
    }
}
