﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ADVManager : SingletonMonoBehaviour<ADVManager>
{
    public ADVController adv_;

    private List<ADVChapter> chapters_;

    public void SetChapter(List<ADVChapter> chapters)
    {
        chapters_ = chapters;
    }

    public void SetChapter(string name)
    {
        var chapter = chapters_.Where(m => m.name_ == name).First();
        adv_.SetADV(chapter);
    }

    public void GoNext()
    {
        adv_.NextSection();

    }

    public bool GetIsADV()
    {
        return adv_.GetIsADV(); ;
    }

    public string GetChapterName()
    {
        return adv_.GetChapterName();
    }

}
