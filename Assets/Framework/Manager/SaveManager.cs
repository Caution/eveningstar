﻿using UnityEngine;
using System.Collections;

public class SaveManager : SingletonMonoBehaviour<SaveManager> {
    const int DATA_NUM = (3);

    /// <summary>
    /// 読み込んでいるデータのID
    /// </summary>
    public int data_id_;

    /// <summary>
    /// セーブデータ情報
    /// </summary>
    public SaveData save_data_;

    void Start() {
        Load(1);
    }

    public void Load() {
        Load(data_id_);
    }

    public void Load(int id) {
        if (id > DATA_NUM)
        {
            Debug.LogError("セーブデータ領域を超えています");
            return;
        }

        data_id_ = id;
        save_data_ = new SaveData(data_id_);
        StartManager.Instance.SetStartPosition(save_data_.start_position_);
        GameStatusManager.LoadScene(save_data_.stage_);

    }

    public void Save() {
        save_data_.Save();
    }

    public void Save(Vector3 position) {
        save_data_.start_position_ = position;
        save_data_.Save();
    }

    public void Reset() {
        PlayerPrefs.DeleteAll();
    }
}
