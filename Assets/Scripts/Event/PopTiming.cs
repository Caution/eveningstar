﻿using UnityEngine;

public class PopTiming : MonoBehaviour {

    public int[] timing_;

    void Start() {
        if (SaveManager.Instance.save_data_ == null)
            return;

        var is_active = false;
        foreach (var item in timing_)
        {
            if (item == SaveManager.Instance.save_data_.scenario_)
                is_active = true;
        }
        gameObject.SetActive(is_active);
    }
}
