﻿using UnityEngine;
using System.Collections;

public class PopAfter : MonoBehaviour {

    public int timing_;

	// Use this for initialization
	void Start () {
        if (SaveManager.Instance.save_data_ == null)
            return;

        gameObject.SetActive(timing_ <= SaveManager.Instance.save_data_.scenario_);
    }

    // Update is called once per frame
    void Update () {
	
	}
}
