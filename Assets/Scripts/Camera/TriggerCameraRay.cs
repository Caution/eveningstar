﻿using UnityEngine;
using System.Collections;

public class TriggerCameraRay : MonoBehaviour {

    public float anime_speed_ = 0.2f; 

    private bool is_hit_ = false;
    private float time_ = 0.0f;
    private Color current_color = Color.white;
    private Color start_color = Color.white;
    private Color hit_color = Color.white * 0.2f;
    private SpriteRenderer sprite_;

	// Use this for initialization
	void Start () {
        is_hit_ = false;
        current_color = Color.white;

        sprite_ = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        if (is_hit_)
            time_ += anime_speed_;
        else
            time_ -= anime_speed_;

        if (time_ > 1.0f)
            time_ = 1.0f;

        if (time_ < -0.0f)
            time_ = 0.0f;

        current_color = Color.Lerp(start_color, hit_color, time_);

        sprite_.color = current_color;
    }
    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "CameraRay")
        {
            is_hit_ = true;
        }
    }

    void OnTriggerStay(Collider collider)
    {
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag == "CameraRay")
        {
            is_hit_ = false;
        }
    }
}
