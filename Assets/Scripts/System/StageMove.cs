﻿using UnityEngine;
using System.Collections;

public class StageMove : MonoBehaviour {

    public Vector3 next_posiiton = Vector3.zero;
    public string next_stage = "";

    void Start() {

#if UNITY_EDITOR
        GetComponent<MeshRenderer>().enabled = true;
#elif true
        GetComponent<MeshRenderer>().enabled = false;
#endif

    }


    void OnTriggerEnter(Collider collider) {
        if (collider.gameObject.tag == "Player")
        {
            StartManager.Instance.SetStartPosition(next_posiiton);
            GameStatusManager.LoadScene(next_stage);
        }
    }

    void OnTriggerStay(Collider collider) {
    }

    void OnTriggerExit(Collider collider) {
    }
}