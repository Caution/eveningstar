﻿using UnityEngine;

public class GameManager : SingletonMonoBehaviour<GameManager> {

    // 良くない
    public bool is_control_ = true;

    // Use this for initialization
    void Start() {
        // 良くない
        is_control_ = true;

    }

    // Update is called once per frame
    void Update() {
        // ゲーム終了
        if (Input.GetKey(KeyCode.Escape))
            GameStatusManager.EndGame();

        if (Input.GetKeyDown(KeyCode.P))
            UIManager.Instance.pause_ui_.SetPause();


        if (Input.GetKeyDown(KeyCode.F11))
        {
            SaveManager.Instance.Reset();
            DataLoad();
        }

        if (Input.GetKeyDown(KeyCode.F12))
            UIManager.Instance.save_ui_.PopUI();

        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Z))
            ADVManager.Instance.GoNext();

        if (GameStatusManager.CurrentScene() != "Title")
        {
            if (SaveManager.Instance.save_data_.hp_ <= 0)
            {
                Destroy(ManagerGroop.Instance);
                GameStatusManager.LoadScene("Title");
            }
        }

        Player.Instance.is_control_ = !ADVManager.Instance.GetIsADV() && is_control_;

        //UIManager.Instance.telop_ui_.SetTelop(
        //    "F11 データリセット F12 セーブ画面 ESC ゲーム終了 " + SaveManager.Instance.save_data_.scenario_ + "データのID" + SaveManager.Instance.save_data_.id_);

    }

    void DataLoad() {
        SaveManager.Instance.Load();
        StartManager.Instance.SetStartPosition(SaveManager.Instance.save_data_.start_position_);
    }
}
