﻿using UnityEngine;
using System.Collections;

public class SaveData
{
    public string id_;
    public int hp_;
    public int bomb_;
    public int money_;
    public int scenario_;
    public string stage_;
    public Vector3 start_position_;

    public SaveData(int id)
    {
        id_ = id.ToString();
        Load();
    }

    public void Load()
    {
        hp_ = PlayerPrefs.GetInt("hp_" + hp_, 5);
        bomb_ = PlayerPrefs.GetInt("bomb_" + id_, 2);
        money_ = PlayerPrefs.GetInt("money_" + id_, 0);
        scenario_ = PlayerPrefs.GetInt("scenario_" + id_, 0);
        stage_ = PlayerPrefs.GetString("stage_" + id_, "TenshiHouse");
        start_position_.x = PlayerPrefs.GetFloat("pos_x_" + id_, -2.66f);
        start_position_.y = PlayerPrefs.GetFloat("pos_y_" + id_, 0.0f);
        start_position_.z = PlayerPrefs.GetFloat("pos_z_" + id_, 2.12f);
    }

    public void Save()
    {
        PlayerPrefs.SetInt("hp_" + id_, hp_);
        PlayerPrefs.SetInt("bomb_" + id_, bomb_);
        PlayerPrefs.SetInt("money_" + id_, money_);
        PlayerPrefs.SetString("stage_" + id_, GameStatusManager.CurrentScene());
        PlayerPrefs.SetInt("scenario_" + id_, scenario_);
        PlayerPrefs.SetFloat("pos_x_" + id_, start_position_.x);
        PlayerPrefs.SetFloat("pos_y_" + id_, start_position_.y);
        PlayerPrefs.SetFloat("pos_z_" + id_, start_position_.z);
        PlayerPrefs.Save();
    }

    public void NewData() {
        PlayerPrefs.SetInt("hp_" + hp_, 5);
        PlayerPrefs.SetInt("bomb_" + id_, 2);
        PlayerPrefs.SetInt("money_" + id_, 0);
        PlayerPrefs.SetString("stage_" + id_, "TenshiHouse");
        PlayerPrefs.SetInt("scenario_" + id_, scenario_);
        PlayerPrefs.SetFloat("pos_x_" + id_, -2.66f);
        PlayerPrefs.SetFloat("pos_y_" + id_, 0.0f);
        PlayerPrefs.SetFloat("pos_z_" + id_, 2.12f);

    }
}
