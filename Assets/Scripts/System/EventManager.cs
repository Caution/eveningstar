﻿public class EventManager : SingletonMonoBehaviour<EventManager> {

    private bool old_adv_is_end_;
    private bool cur_adv_is_end_;
    private bool is_scenario = false;

    void Start() {
        old_adv_is_end_ = false;
        cur_adv_is_end_ = false;
        is_scenario = false;

    }

    void Update() {
        cur_adv_is_end_ = ADVManager.Instance.adv_.GetIsADV();

        switch (SaveManager.Instance.save_data_.scenario_)
        {
            case 0:
                NextScenario("story01");
                break;
            default:
                break;
        }

        if (is_scenario)
        {
            if (old_adv_is_end_ && !cur_adv_is_end_)
            {
                is_scenario = false;
                SaveManager.Instance.save_data_.scenario_++;
            }
        }

        old_adv_is_end_ = ADVManager.Instance.adv_.GetIsADV();
    }

    public void NextScenario(string chapter) {
        if (SaveManager.Instance.save_data_.scenario_ == 0)
        {
            SaveManager.Instance.save_data_.scenario_++;
        }
        else
        {
            is_scenario = true;
        }
            ScenarioManager.Instance.SetChapter(chapter);
    }
}
