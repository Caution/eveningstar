﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScenarioManager : SingletonMonoBehaviour<ScenarioManager>
{

    // Use this for initialization
    void Start()
    {
        var chapter_list = new List<ADVChapter>();

        var files = Resources.LoadAll<TextAsset>("Text");
        foreach (var item in files)
            chapter_list.Add(new ADVChapter(item.name,item.text));

        ADVManager.Instance.SetChapter(chapter_list);

    }

    public void SetChapter(string chapter)
    {
        ADVManager.Instance.SetChapter(chapter);

    }

    // Update is called once per frame
    void Update()
    {

    }
}
