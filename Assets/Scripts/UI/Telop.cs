﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Telop : MonoBehaviour {

    private Text text_;

	// Use this for initialization
	void Start () {
        text_ = GetComponent<Text>();
    }
	
    public void SetTelop(string value)
    {
        text_.text = value;
    }

}
