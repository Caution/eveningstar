﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : SingletonMonoBehaviour<UIManager>
{
    public GameObject normal_ui_;
    public ADVController adv_ui_;
    public PauseController pause_ui_;
    public SaveController save_ui_;
    public Telop telop_ui_;

    public string GetActivUI() {
        if (adv_ui_.GetIsADV())
            return "adv";

        if(pause_ui_.is_pause_)
            return "pause";

        if (save_ui_.is_pause_)
            return "save";

        return "nothing";
    }
}
