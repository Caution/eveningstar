﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SaveItem : MonoBehaviour {

    public int id_;
    public Text information_;

    public void Start() {
        SetInfomation();
    }

    public void Save() {
        SaveManager.Instance.save_data_.id_ = id_.ToString();
        SaveManager.Instance.Save(Player.Instance.transform.position);
        SetInfomation();
    }

    public void Load() {
        SaveManager.Instance.Load(id_);
        UIManager.Instance.save_ui_.PopUI();
        SetInfomation();
    }

    void SetInfomation() {
        var save_data = new SaveData(id_);
        if (save_data.id_ != "0")
            information_.text = string.Format("Data {0}\n シナリオ{1} 場所{2}", save_data.id_, save_data.scenario_, save_data.stage_);
        else
            information_.text = string.Format("Data {0}\n データ無し", save_data.id_);
    }
}
