﻿using UnityEngine;
using System.Collections;

public class SaveController : MonoBehaviour {

    public bool is_pause_ = false;
    public float time_speed_ = -0.08f;
    private LerpVector3 scal_lerp_;

    // Use this for initialization
    void Start () {
        scal_lerp_ = new LerpVector3(time_speed_);
        scal_lerp_.SetPalamater(Vector3.zero, Vector3.one);
        is_pause_ = false;
    }

    // Update is called once per frame
    void Update() {
        scal_lerp_.Update();
        transform.localScale = scal_lerp_.Lerp();
    }

    public void PopUI()
    {
        scal_lerp_.SpeedRebirth();
        is_pause_ = !is_pause_;
        if(is_pause_)
            GameStatusManager.Instance.StopGame();
        else
            GameStatusManager.Instance.ActivateGame();
    }
}
