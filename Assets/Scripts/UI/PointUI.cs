﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PointUI : MonoBehaviour {

    private Text string_point_;

    // Use this for initialization
    void Start () {
        string_point_ = GetComponent<Text>();

    }

    // Update is called once per frame
    void Update () {
        string_point_.text = MoneyManager.Instance.GetMoney().ToString();

    }
}
