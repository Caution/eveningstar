﻿using UnityEngine;
using System.Collections;

public class BombCounter : MonoBehaviour {

    public GameObject[] bomb_ui_;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        var bomb_num = SaveManager.Instance.save_data_.bomb_;

        var coutner = 0;
        foreach (var item in bomb_ui_)
        {
            bomb_ui_[coutner].SetActive(coutner < bomb_num);
            coutner++;
        }

    }
}
