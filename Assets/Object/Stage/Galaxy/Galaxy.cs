﻿using UnityEngine;
using System.Collections;

public class Galaxy : MonoBehaviour {

    public Material galaxy_mat_;
    private Material first_material_;
    private Player player_;
    private bool is_end_story_;

    // Use this for initialization
    void Start() {
        player_ = Player.Instance;
        var syk_box = player_.main_camera_.GetComponent<Skybox>();
        first_material_ = syk_box.material;
        syk_box.material = galaxy_mat_;

    }

    // Update is called once per frame
    void Update() {
        GameManager.Instance.is_control_ = true;
        if (SaveManager.Instance.save_data_.scenario_ == 22)
        {
        }
    }

    void OnDestroy() {
        var syk_box = Player.Instance.main_camera_.GetComponent<Skybox>();
        syk_box.material = first_material_;
    }
}
