﻿using UnityEngine;
using System.Collections;

public class Dugeon : MonoBehaviour {

    public Material galaxy_mat_;
    public GameObject sword_stand_;
    public GameObject particle_;
    private Material first_material_;
    private Player player_;
    private bool is_end_story_;

    // Use this for initialization
    void Start() {
        player_ = Player.Instance;
        var syk_box = player_.main_camera_.GetComponent<Skybox>();
        first_material_ = syk_box.material;
        syk_box.material = galaxy_mat_;

    }

    // Update is called once per frame
    void Update() {
        var adv_instance = ADVManager.Instance;
        var is_adv = adv_instance.adv_.GetIsADV();
        var current_story = adv_instance.GetChapterName();

        if (!is_end_story_)
        {
            if (is_adv)
            {
                is_end_story_ = true;
            }
        }
        else
        {
            if (!is_adv)
            {
                if (current_story == "story21")
                {
                }
                is_end_story_ = false;
            }
        }

        // 良くない
        if (is_end_story_)
        {

        }
        GameManager.Instance.is_control_ = true;
        if (SaveManager.Instance.save_data_.scenario_ == 20 || SaveManager.Instance.save_data_.scenario_ == 21)
        {
            player_.direction_id_ = 0;
            GameManager.Instance.is_control_ = false;
            var move_speed = 10.0f * Time.deltaTime;
            if (current_story == "story21")
                move_speed *= 40.0f;
            player_.transform.position += Vector3.up * move_speed;
            sword_stand_.transform.position += Vector3.up * move_speed;

            if (sword_stand_.transform.position.y > 350 && SaveManager.Instance.save_data_.scenario_ == 20)
            {
                player_.transform.position -= Vector3.up * 300;
                sword_stand_.transform.position -= Vector3.up * 300;
            }
            else
            {
                if (SaveManager.Instance.save_data_.scenario_ == 21)
                    particle_.SetActive(true);
            }

        }


    }

    void OnDestroy() {
        var syk_box = Player.Instance.main_camera_.GetComponent<Skybox>();
        syk_box.material = first_material_;
    }
}
