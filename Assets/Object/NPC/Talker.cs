﻿using UnityEngine;
using System.Collections;

public class Talker : MonoBehaviour
{
    public SpriteRenderer z_icon_ = null;
    public SpriteRenderer event_icon_ = null;

    // Use this for initialization
    virtual protected void Start()
    {
        if (event_icon_ != null)
            event_icon_.enabled = true;

    }

    // Update is called once per frame
    void Update()
    {

    }

    virtual public void Talk()
    {

    }

    virtual public void OnTriggerStay(Collider collider)
    {
        if (collider.tag == "Player")
        {
            if (z_icon_ != null)
                z_icon_.enabled = true;
            if (event_icon_ != null)
                event_icon_.enabled = false;
        }
    }

    virtual public void OnTriggerExit(Collider collider)
    {
        if (collider.tag == "Player")
        {
            if (z_icon_ != null)
                z_icon_.enabled = false;
            if (event_icon_ != null)
                event_icon_.enabled = true;
        }
    }
}
