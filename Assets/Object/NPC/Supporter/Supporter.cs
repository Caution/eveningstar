﻿using UnityEngine;
using System.Collections;

public class Supporter : MonoBehaviour {

    public float chase_speed_ = 0.07f;
    private Animator animator_;

	// Use this for initialization
	void Start () {
        
        animator_ = GetComponent<Animator>();

    }
	
	// Update is called once per frame
	void Update () {
        var player = Player.Instance;
        var playerDistance = (player.transform.position - transform.position).normalized;
        if (Vector3.Distance(player.transform.position , transform.position) > 1.0f)
        {
            transform.position += playerDistance * chase_speed_ * GameStatusManager.Instance.delta_time_;
            if (playerDistance.x > 0.5f)
                animator_.SetInteger("directional", 2);
            else if (playerDistance.x < -0.5f)
                animator_.SetInteger("directional", 3);
            else if (playerDistance.z > 0.5f)
                animator_.SetInteger("directional", 0);
            else if (playerDistance.z < -0.5f)
                animator_.SetInteger("directional", 1);
        }

    }
}
