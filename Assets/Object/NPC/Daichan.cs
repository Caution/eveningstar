﻿using UnityEngine;
using System.Collections;

public class Daichan : Talker {

    private bool is_fly_ = false;

    override protected void Start() {
        base.Start();
        is_fly_ = false;
    }

    void Update() {
        if (is_fly_ && !ADVManager.Instance.GetIsADV())
            transform.position += Vector3.up * GameStatusManager.Instance.delta_time_ * 0.1f;
    }

    override public void Talk() {
        if (SaveManager.Instance.save_data_.scenario_ == 5)
        {
            is_fly_ = true;
            EventManager.Instance.NextScenario("story06");
        }
    }
}