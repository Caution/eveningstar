﻿using UnityEngine;
using System.Collections;

public class ReimuTalk : Talker {
    // Use this for initialization
    override protected void Start() {
        base.Start();
    }

    override public void Talk() {
        switch (SaveManager.Instance.save_data_.scenario_)
        {
            case 1:
                EventManager.Instance.NextScenario("story02");
                break;
            default:
                ADVManager.Instance.SetChapter("霊夢");
                break;
        }

    }

}