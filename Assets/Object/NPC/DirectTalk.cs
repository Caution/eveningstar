﻿using UnityEngine;
using System.Collections;

public class DirectTalk : Talker {

    public string chapter_;

    // Use this for initialization
    override protected void Start() {
        base.Start();

    }

    // Update is called once per frame
    void Update() {

    }

    override public void Talk() {
        ADVManager.Instance.SetChapter(chapter_);
    }
}
