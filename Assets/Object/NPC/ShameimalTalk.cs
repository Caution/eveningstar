﻿using UnityEngine;
using System.Collections;

public class ShameimalTalk : Talker {
    // Use this for initialization
    override protected void Start() {
        base.Start();

    }

    override public void Talk() {
        switch (SaveManager.Instance.save_data_.scenario_)
        {
            case 6:
                EventManager.Instance.NextScenario("story07");
                break;
            default:
                ADVManager.Instance.SetChapter("射命丸");
                break;
        }

    }

}