﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MiniMap : MonoBehaviour {

    public GameObject point_;

    private Dictionary<string, Vector3> pont_list_ = new Dictionary<string, Vector3>()
        {
        {"TenshiHouse",new Vector3(-87,220,0) },
        {"Jinja",new Vector3(-6,220,0)},
        {"JinjaRoad",new Vector3(-6,144,0) },
        {"Village",new Vector3(0,34,0) },
        {"MysticRoad",new Vector3(-3,-55,0)},
        {"Remains",new Vector3(-3,-112,0) },
        {"Dungeon",new Vector3(-3,-195.1f,0) },
        {"BambooRoad",new Vector3(77,39,0) },
        {"BambooForest",new Vector3(136,39,0)},
        {"BambooBoss",new Vector3(212,39,0)},
        {"LakeRoad",new Vector3(-92,39,0) },
        {"Lake",new Vector3(-188,39,0) }
    };

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        var rect_transform = point_.GetComponent<RectTransform>();
        var current_scene = GameStatusManager.CurrentScene();
        if (current_scene == "Galaxy" || current_scene == "Title")
            return;
        var set_position = pont_list_[GameStatusManager.CurrentScene()];
        rect_transform.localPosition = set_position;

    }
}
