﻿using UnityEngine;
using System.Collections;

public class Player : SingletonMonoBehaviour<Player>
{
    /// <summary>
    /// コンポーネント情報格納変数
    /// </summary>
    private CharacterController controller_;
    private Animator animator_;

    /// <summary>
    /// メインカメラ
    /// </summary>
    public Camera main_camera_;

    /// <summary>
    /// 固定ステータス
    /// </summary>
    public float fall_speed_ = 2.0f;
    public float walk_speed_ = 2.0f;
    public float dash_speed_ = 2.0f;
    public float jump_speed_ = 2.0f;


    /// <summary>
    /// アニメーション用メンバ
    /// </summary>
    public int direction_id_ = 0;
    public bool is_command_action_ = false;
    public int sword_id_ = 0;
    public int sword_id_old_ = 0;
    public bool is_damage = false;

    /// <summary>
    /// アニメーションのパラメータID
    /// </summary>
    private int shot_param_ = 0;
    private int sword_param_ = 0;
    private int direction_param_ = 0;
    private int damage_param_ = 0;

    private Vector3 player_direction_ = Vector3.back;

    /// <summary>
    /// 爆弾系メンバ
    /// </summary>
    public GameObject bomb_;
    public float bomb_set_distance_ = 5.0f;

    /// <summary>
    /// 弾丸系メンバ
    /// </summary>
    public GameObject bullet_;
    public float bullet_set_distance_ = 1.0f;

    public bool is_control_ = true;

    public float shot_time_ = 0.0f;
    public float shot_interval_ = 1.5f;
    public float jump_value_ = 1.0f;
    public float jump_inartia_ = 0.8f;
    private float jump_force_ = 0.0f;
    private Vector3 dash_attack_force_ = Vector3.zero;

    private Vector3 nock_buck_ = Vector3.zero;
    public float nock_buck_inartia_ = 0.5f;

    // Use this for initialization
    void Start()
    {
        // 必要なコンポーネント情報を確保
        animator_ = GetComponent<Animator>();
        controller_ = GetComponent<CharacterController>();

        is_command_action_ = false;
        direction_id_ = 0;
        sword_id_ = 0;

        sword_param_ = Animator.StringToHash("sword");
        shot_param_ = Animator.StringToHash("is_shot");
        direction_param_ = Animator.StringToHash("direction");
        damage_param_ = Animator.StringToHash("is_damage");
    }

    // Update is called once per frame
    void Update()
    {
        if (is_control_ && GameStatusManager.Instance.is_game_active_)
        {
            Move();
            CommandAction();
        }

        // アニメーションの更新
        animator_.SetBool(damage_param_, is_damage);
        animator_.SetBool(shot_param_, is_command_action_);
        animator_.SetInteger(direction_param_, direction_id_);
        animator_.SetInteger(sword_param_, sword_id_);
    }

    private void Move()
    {
        var delta_time = GameStatusManager.Instance.delta_time_;
        var move_vector = Vector3.zero;
        var is_dash = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);

        if (is_command_action_ == false && sword_id_ == 0)
        {
            if (Input.GetKey(KeyCode.UpArrow))
            {
                player_direction_ = Vector3.forward;
                move_vector.z += 1.0f * delta_time;
                direction_id_ = 1;
            }

            if (Input.GetKey(KeyCode.DownArrow))
            {
                player_direction_ = Vector3.back;
                move_vector.z -= 1.0f * delta_time;
                direction_id_ = 0;
            }

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                player_direction_ = Vector3.left;
                move_vector.x -= 1.0f * delta_time;
                direction_id_ = 3;
            }

            if (Input.GetKey(KeyCode.RightArrow))
            {
                player_direction_ = Vector3.right;
                move_vector.x += 1.0f * delta_time;
                direction_id_ = 2;
            }
        }
        else if (sword_id_old_ != sword_id_ && is_dash)
        {
            float move_speed = 2.0f * delta_time;
            if (Input.GetKey(KeyCode.UpArrow))
                dash_attack_force_.z += move_speed;
            if (Input.GetKey(KeyCode.DownArrow))
                dash_attack_force_.z -= move_speed;
            if (Input.GetKey(KeyCode.LeftArrow))
                dash_attack_force_.x -= move_speed;
            if (Input.GetKey(KeyCode.RightArrow))
                dash_attack_force_.x += move_speed;
        }

        dash_attack_force_ *= 0.8f;

        // ダッシュ
        if (is_dash)
        {
            move_vector = move_vector.normalized * (walk_speed_ * dash_speed_);

            // アニメーション速度を上げる(移動中以外は元に戻す)
            if (is_command_action_ == false && sword_id_ == 0)
                animator_.speed = 2;
            else
                animator_.speed = 1;
        }
        else
        {
            move_vector = move_vector.normalized * walk_speed_;
            animator_.speed = 1;
        }

        if (!controller_.isGrounded)
            if (sword_id_ > 0)
                animator_.speed = 0.5f;

        if (controller_.isGrounded)
        {
            if (Input.GetKey(KeyCode.Space))
            {
                jump_force_ = jump_value_;
            }
        }

        move_vector += dash_attack_force_;
        move_vector.y += jump_force_;
        move_vector.y -= fall_speed_;
        jump_force_ *= jump_inartia_;
        nock_buck_ *= nock_buck_inartia_;
        is_damage = nock_buck_.magnitude > 0.2f;
        controller_.Move((move_vector * delta_time) + nock_buck_);

    }

    private void CommandAction()
    {
        sword_id_old_ = sword_id_;
        if (is_command_action_ == false && sword_id_ == 0)
        {
            if (shot_time_ + shot_interval_ < Time.time)
            {
                if (Input.GetKey(KeyCode.Z))
                {
                    shot_time_ = Time.time;
                    Vector3 set_pos =
                        (player_direction_ * bullet_set_distance_) + transform.position;
                    GameObject bullet = (GameObject)Instantiate(bullet_, set_pos, transform.rotation);
                    bullet.GetComponent<Bullet>().SetDirection(player_direction_);
                }
                else if (Input.GetKeyDown(KeyCode.C))
                {
                    if(SaveManager.Instance.save_data_.bomb_ > 0)
                    {
                        SaveManager.Instance.save_data_.bomb_--;
                        shot_time_ = Time.time;
                        Instantiate(bomb_, transform.position, transform.rotation);
                    }
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            sword_id_ = 1;

        }
    }

    void OnTriggerStay(Collider collider)
    {
        if (collider.tag == "Murabito")
        {
            var talker = collider.GetComponent<Talker>();

            if (talker != null && ADVManager.Instance.GetIsADV() == false)
                if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Z))
                    talker.Talk();

        }
    }

    public void BeginShot()
    {
    }

    public void EndShot()
    {
        is_command_action_ = false;
    }

    public void SwordEnd()
    {
        sword_id_ = 0;
    }

    void OnControllerColliderHit(ControllerColliderHit collider) {

        if(collider.gameObject.tag == "EventWall")
        {
            var event_wall = collider.gameObject.GetComponent<EventBack>();
            event_wall.Hit();
        }

        if (collider.gameObject.tag == "Enemy")
        {
            var enemy = collider.gameObject.GetComponent<EnemyBase>();
            if (enemy.is_attack_)
            {
                var nock_buck_vector = transform.position - collider.gameObject.transform.position;

                nock_buck_ = nock_buck_vector.normalized * 5.0f;
                SaveManager.Instance.save_data_.hp_--;

            }

        }
    }
}
