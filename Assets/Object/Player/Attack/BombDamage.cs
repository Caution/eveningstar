﻿using UnityEngine;
using System.Collections;

public class BombDamage : PlayerAttack {

    private bool is_deastroy_;

	// Use this for initialization
	void Start () {
        is_deastroy_ = false;

    }
	
	// Update is called once per frame
	void Update () {
        if (is_deastroy_)
            Destroy(gameObject);
        is_deastroy_ = true;

    }
}
