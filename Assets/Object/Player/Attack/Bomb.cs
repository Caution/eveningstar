﻿using UnityEngine;
using System.Collections;

public class Bomb : PlayerAttack {

    public GameObject explosion_;
    public GameObject dameger_;
    public float explode_time_ = 1.0f;

	void Start () {
        Destroy(gameObject, explode_time_);
	}


    void OnDestroy()
    {
        GameObject effect = 
        (GameObject)Instantiate(explosion_, transform.position, transform.rotation);
        Destroy(effect, 2.0f);
        Instantiate(dameger_, transform.position, transform.rotation);
    }
}
