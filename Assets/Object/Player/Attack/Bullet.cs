﻿using UnityEngine;
using System.Collections;

public class Bullet : PlayerAttack
{

    /// <summary>
    /// コンポーネント情報格納変数
    /// </summary>
    private Rigidbody rigidbody_;

    public Vector3 direction_;
    public float move_speed_ = 10.0f;

    // Use this for initialization
    void Start()
    {
        rigidbody_ = GetComponent<Rigidbody>();

        Destroy(gameObject, 3.0f);
    }

    // Update is called once per frame
    void Update()
    {
        rigidbody_.velocity = direction_ * move_speed_ * GameStatusManager.Instance.delta_time_;
        if (is_hit_ == true)
            Destroy(gameObject);
    }

    public void SetDirection(Vector3 direction)
    {
        if (direction.x > 0.5f)
        {
            transform.eulerAngles = Vector3.forward * 90;
            transform.position += Vector3.up * 0.6f;
        }
        if (direction.x < -0.5f)
        {
            transform.eulerAngles = Vector3.forward * 270;
            transform.position += Vector3.up * 0.6f;
        }
        if (direction.z > 0.5f)
        {
            transform.eulerAngles = Vector3.right * 320 + Vector3.forward * 180 + Vector3.up * 180;
            transform.position += Vector3.up * 1.2f;
        }
        if (direction.z < -0.5f)
        {
            transform.eulerAngles = Vector3.right * 70;
        }
        
        direction_ = direction;

    }
}
