﻿using UnityEngine;
using System.Collections;

public class Slime : EnemyBase{

    public float move_speed_ = 100.0f;

    /// <summary>
    /// コンポーネント情報格納変数
    /// </summary>
    private Animator animator_;
    private Rigidbody rigidbody_;
    private BoxCollider box_collider_;

    public bool is_left_ = true;
    private int is_left_param_;

    private bool is_move = false;

    // Use this for initialization
    void Start () {
        hp_ = 50;
        is_attack_ = true;

        // 必要なコンポーネント情報を確保
        animator_ = GetComponent<Animator>();
        rigidbody_ = GetComponent<Rigidbody>();
        box_collider_ = GetComponent<BoxCollider>();

        is_left_param_ = Animator.StringToHash("is_left");
        if (!is_left_)
        {
            Vector3 set_center = box_collider_.center;
            set_center.x = 1.28f;
            box_collider_.center = set_center;
        }

        if (Random.Range(0, 10) <= 0)
            ChengeDirection();
    }

    // Update is called once per frame
    override protected void Update () {
	    if (is_move)
        {
            if (is_left_)
                rigidbody_.AddForce(Vector3.left * move_speed_ * GameStatusManager.Instance.delta_time_);
            else
                rigidbody_.AddForce(Vector3.right * move_speed_ * GameStatusManager.Instance.delta_time_);
        }

        animator_.SetBool(is_left_param_, is_left_);
        base.Update();
    }

    public void BeginMove()
    {
        is_move = true;
    }

    public void EndMove()
    {
        is_move = false;
    }

    /// <summary>
    /// 反転するかもしれない
    /// </summary>
    public void ReversalMight()
    {
        if (Random.Range(0, 10) <= 1)
        {
            ChengeDirection();

        }
    }

    private void ChengeDirection() {
        is_left_ = !is_left_;
        Vector3 set_center = box_collider_.center;
        set_center.x = -set_center.x;
        box_collider_.center = set_center;
    }
}
