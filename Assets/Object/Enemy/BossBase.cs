﻿using UnityEngine;
using System.Collections;

public class BossBase : EnemyBase {

    public string story_id_;

    // Use this for initialization
    void Start() {
    }

    // Update is called once per frame
    override protected void Update() {
        if (hp_ <= 0)
        {
            EventManager.Instance.NextScenario("story" + story_id_.ToString().PadLeft(2, '0'));
        }

        base.Update();
    }


}
