﻿using UnityEngine;
using System.Collections;

public class EnemyBase : MonoBehaviour {

    public GameObject destroy_effect_;
    public GameObject[] drop_item_;
    public int hp_ = 10;
    public int power_ = 1;
    public bool is_attack_;

	// Use this for initialization
	void Start () {
        is_attack_ = false;

    }
	
	// Update is called once per frame
	virtual protected void Update () {
	    if (hp_ <= 0)
        {
            var effect = Instantiate(destroy_effect_, transform.position, transform.rotation);
            Destroy(effect, 2.0f);

            foreach (var item in drop_item_)
                Instantiate(item, transform.position, transform.rotation);
            Destroy(gameObject);

        }
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "PlayerAttack")
        {
            PlayerAttack attack_object = col.GetComponent<PlayerAttack>();
            hp_ -= attack_object.damage_;
            attack_object.is_hit_ = true;

        }
    }
}
