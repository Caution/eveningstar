﻿using UnityEngine;
using System.Collections;

public class BambooBoss : MonoBehaviour {

    private bool is_end_story_;

	// Use this for initialization
	void Start () {
        is_end_story_ = false;

    }
	
	// Update is called once per frame
	void Update () {

        var is_adv = ADVManager.Instance.adv_.GetIsADV();

        if(!is_end_story_)
        {
            if (is_adv)
            {
                is_end_story_ = true;
            }
        }
        else
        {
            if (!is_adv)
            {
                StartManager.Instance.SetStartPosition(new Vector3(75.0f,0.0f,30.0f));
                GameStatusManager.LoadScene("Village");
            }
        }
    }
}
