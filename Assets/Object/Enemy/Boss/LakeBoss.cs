﻿using UnityEngine;
using System.Collections;

public class LakeBoss : MonoBehaviour {

    public GameObject oku;
    public GameObject namazu;
    private bool is_end_story_;

    // Use this for initialization
    void Start () {
        is_end_story_ = false;

    }

    // Update is called once per frame
    void Update () {
        var adv_instance = ADVManager.Instance;
        var is_adv = adv_instance.adv_.GetIsADV();
        var current_story = adv_instance.GetChapterName();

        if (!is_end_story_)
        {
            if (is_adv)
            {
                is_end_story_ = true;
            }
        }
        else
        {
            if (!is_adv)
            {
                if(current_story == "story15")
                {
                    var set_transform = oku.transform;
                    set_transform.position += Vector3.forward * 10.0f;
                    Instantiate(namazu, set_transform.position, Quaternion.identity);
                    oku.SetActive(false);

                }
                else if (current_story == "story16")
                {
                    StartManager.Instance.SetStartPosition(new Vector3(75.0f, 0.0f, 30.0f));
                    GameStatusManager.LoadScene("Village");
                }
                is_end_story_ = false;
            }
        }

    }
}
