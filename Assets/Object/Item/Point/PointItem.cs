﻿using UnityEngine;
using System.Collections;

public class PointItem : MonoBehaviour {

    public int point_value_ = 10;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	}

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            if(SaveManager.Instance.save_data_.bomb_ < 8)
            {
                SaveManager.Instance.save_data_.bomb_++;
                Destroy(gameObject);
            }
        }
    }
}
