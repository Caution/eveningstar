﻿using UnityEngine;
using System.Collections;

public class EventArea : MonoBehaviour {

    public int story_id_;

    void Start() {

#if UNITY_EDITOR
        GetComponent<MeshRenderer>().enabled = true;
#elif true
        GetComponent<MeshRenderer>().enabled = false;
#endif

    }

    void Update() {
        if (story_id_ - 1 < SaveManager.Instance.save_data_.scenario_)
            Destroy(gameObject);

    }

    void OnTriggerStay(Collider collider)
    {
        if (story_id_ -1  == SaveManager.Instance.save_data_.scenario_)
        {
            EventManager.Instance.NextScenario("story" + story_id_.ToString().PadLeft(2, '0'));
            Destroy(gameObject);
        }
    }
}
