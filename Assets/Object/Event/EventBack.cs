﻿using UnityEngine;
using System.Collections;

public class EventBack : MonoBehaviour {

    public int story_id_;
    public bool is_talk_ = true;
    public string chapter_;

    void Start() {
        GetComponent<MeshRenderer>().enabled = false;
    }

    void Update() {
        if (SaveManager.Instance.save_data_.scenario_ >= story_id_)
        {
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// プレイヤコントローラの当たり判定
    /// </summary>
    public void Hit() {
        if (SaveManager.Instance.save_data_.scenario_ < story_id_)
        {
            if (is_talk_)
                ADVManager.Instance.SetChapter(chapter_);
        }
    }

}
