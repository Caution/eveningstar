﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections;

public class RedBallCounter : MonoBehaviour {

    public Sprite on_;
    public Sprite off_;
    public int[] ids_;
    public Image[] balls_;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        var sceneario =  SaveManager.Instance.save_data_.scenario_;

        var coutner = 0;
        foreach(var item in ids_)
        {
            if (item < SaveManager.Instance.save_data_.scenario_)
                balls_[coutner].sprite = on_;
            else
                balls_[coutner].sprite = off_;
            coutner++;

        }
    }
}
