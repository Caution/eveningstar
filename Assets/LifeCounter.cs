﻿using UnityEngine;
using System.Collections;

public class LifeCounter : MonoBehaviour {

    public GameObject[] life_ui_;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        var life_num = SaveManager.Instance.save_data_.hp_;

        var coutner = 0;
        foreach (var item in life_ui_)
        {
            life_ui_[coutner].SetActive(coutner < life_num);
            coutner++;
        }
    }
}
